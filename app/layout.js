import "./globals.css";
import { Navbar } from "./components/navbar/NavBar";

export default function RootLayout({ children }) {
  return (
    <html className={`bg-neutral-900`}>
      <head>
        <title>Nicolás</title>
      </head>

      <body >
        <Navbar />
        <div >{children}</div>
      </body>
    </html>
  );
}
