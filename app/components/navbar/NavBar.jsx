import styles from "./navbar.module.css";

const sideBarContent = [
  {
    label: "Home",
    route: "/",
  },
  { label: "Wordle", route: "/wordle" },
  { label: "Tetris", route: "/tetris" },
];

export function Navbar() {
  return (
    <div className="drawer">
      {/* Drawer Button */}
      <input id="my-drawer-3" type="checkbox" className="drawer-toggle" />
      <div className="drawer-content flex flex-col">
        {/* Navbar */}
        <div className={`${styles.test} navbar bg-emerald-500`}>
          <div className="navbar-start">
            <label
              htmlFor="my-drawer-3"
              className="btn btn-square btn-ghost text-white hover:text-neutral-900"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-6 h-6 stroke-current neutral-900"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </label>
          </div>
          <div className="navbar-center ">
            <a href="/" className="font-semibold text-xl -ml-12 text-white">Nicolás</a>
          </div>
        </div>
      </div>
      <div className="drawer-side z-40">
        <label htmlFor="my-drawer-3" className="drawer-overlay"></label>
        <ul className="menu p-4 w-80 h-full bg-neutral-900">
          {/* Sidebar content here */}
          <ul>
            {sideBarContent.map((content) => {
              return (
                <li key={content.label}>
                  <a href={content.route}>{content.label}</a>
                </li>
              );
            })}
          </ul>
        </ul>
      </div>
    </div>
  );
}
