import styles from "../../wordle/wordle.module.css";

export default function Grid({ rows, currentRow }) {
  const letterCount = 5;
  const letterContainers = [];

  for (let i = 0; i < letterCount; i++) {
    const letter = rows[i] ? rows[i] : "";

    const letterContainerClasses = `
      border border-neutral-500 border-2 
      flex items-center justify-center w-16 h-16 rounded-sm`;

    const letterContainer = (
      <div
        id={`row${currentRow},${i}`}
        key={i}
        className={
          letter
            ? `${styles.letterContainer + letterContainerClasses}`
            : letterContainerClasses
        }
      >
        <a className="uppercase font-bold text-xl text-white">{letter}</a>
      </div>
    );

    letterContainers.push(letterContainer);
  }

  return <>{letterContainers}</>;
}
