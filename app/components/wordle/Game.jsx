import { useState, useEffect } from "react";
import {
  correct,
  incorrect,
  closeEnough,
  shake,
  keys,
} from "../../wordle/wordle.module.css";
import { wordList } from "../../../public/wordleData";
import Grid from "./Grid";

const wordlistLenght = 1075;
const word = wordList[Math.floor(Math.random() * wordlistLenght)];

/*
Styling the cells by their status (correct/incorrect or wrongly positioned) 
CurrentRow: self explainatory
word: the solution
answer: What the user put
index: index of the letter in the answer, take it as answer[index]
*/
function addStyling(currentRow, word, answer, index) {
  const cell = document.getElementById(`row${currentRow},${index}`);
  const key = document.getElementById(answer[index].toUpperCase());
  // correct (matched) index letter
  if (answer[index] === word[index]) {
    cell.classList.add(correct);
    key.classList.contains(incorrect)
      ? key.classList.remove(incorrect)
      : key.classList.contains(closeEnough)
      ? key.classList.remove(closeEnough)
      : {};
    key.classList.add(correct);
    return;
  }

  let wrongPosition = 0;
  let wrongLetter = 0;
  const wordLength = 5;
  for (let i = 0; i < wordLength; i++) {
    //Counting how many times its in the wrong position
    if (word[i] === answer[index] && answer[i] !== answer[index]) {
      wrongPosition++;
    }

    //Counting how many times the letter is wrong
    if (i <= index) {
      if (answer[i] === answer[index] && word[i] !== answer[index]) {
        wrongLetter++;
      }
    }

    // an unmatched answer letter is wrong if it pairs with
    // an unmatched word letter
    if (i >= index) {
      if (wrongLetter === 0) {
        break;
      }
      if (wrongLetter <= wrongPosition) {
        cell.classList.add(closeEnough);
        key.classList.contains(incorrect)
          ? key.classList.remove(incorrect)
          : key.classList.contains(correct)
          ? {}
          : key.classList.add(closeEnough);
        return;
      }
    }
  }

  // otherwise not any
  cell.classList.add(incorrect);
  key.classList.contains(closeEnough)
    ? {}
    : key.classList.contains(correct)
    ? {}
    : key.classList.add(incorrect);
}

function isInWordList(str, array) {
  return array.includes(str);
}

//Normalizes the user's input
function normalizeInput(inputString) {
  //Used to keep any ñs in the word
  let enie = 0;

  inputString = inputString.join("").toLowerCase();
  inputString.includes("ñ") ? (enie = inputString.indexOf("ñ")) : {};
  inputString = inputString.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

  if (enie > 0) {
    let tempInput = inputString.split("");
    tempInput[enie] = "ñ";
    return tempInput.join("");
  } else return inputString;
}

export default function Wordle({ updateGameState }) {
  const [rows, setRows] = useState([[], [], [], [], [], []]); //Rows
  const [tries, setTries] = useState(0); //How many tries
  const [gameStatus, setGameStatus] = useState(false); //Win-lose status

  function addLetter(e) {
    rows[tries] = [...rows[tries], e.key];
    setRows([...rows]);
  }

  const handleKeyDown = (e) => {
    //Removes the last letter from the current guess
    if (e.key === "Backspace" && rows[tries].length > 0) {
      rows[tries].pop();
      setRows([...rows]);
    }

    //Checks if its an alpha character and adds it to the guesses
    else if (
      rows[tries].length < 5 &&
      e.key !== "Backspace" &&
      ((e.keyCode >= 65 && e.keyCode < 91) ||
        (e.keyCode >= 97 && e.keyCode <= 122))
    ) {
      addLetter(e);
    }

    /*Sends the answer to check its validity
		Answering wrong increments the number of tries*/
    if (e.key == "Enter" && rows[tries].length == 5 && tries < 6) {
      let validWord = false; //keeps track of the validity of the users input

      //Converts and normalizes the input
      const answer = normalizeInput(rows[tries]);

      if (isInWordList(answer, wordList)) {
        validWord = true;

        for (let i = 0; i < 5; i++) addStyling(tries, word, answer, i);

        //Checks if the answer is correct
        if (answer.localeCompare(word, "es", { sensitivity: "base" })) {
          //Incrementing the number of tries
          setTries(tries + 1);
        } else {
          //Sets the gameSatus to true, meaning that the player won
          setGameStatus(true);
        }
      } else {
        //Toggles the shake animation
        const row = document.getElementById(`row${tries}`);
        row.classList.toggle(shake);
        validWord = false;
      }
    }
  };

  useEffect(() => {
    //Checks if the player lost the game
    if (tries >= 6 && !gameStatus) {
      updateGameState(tries, 0, word, true);
    } else if (gameStatus) {
      updateGameState(tries, true, word, true);
    } else {
      handleKeyDown;
      //Adds an event listener
      document.addEventListener("keydown", handleKeyDown);

      //Removes the event listener
      return () => {
        document.removeEventListener("keydown", handleKeyDown);
      };
    }
  });
  const keyboardRows = [
    [
      { key: "Q", keyCode: 81 },
      { key: "W", keyCode: 87 },
      { key: "E", keyCode: 69 },
      { key: "R", keyCode: 82 },
      { key: "T", keyCode: 84 },
      { key: "Y", keyCode: 89 },
      { key: "U", keyCode: 85 },
      { key: "I", keyCode: 73 },
      { key: "O", keyCode: 79 },
      { key: "P", keyCode: 80 },
    ],
    [
      { key: "A", keyCode: 65 },
      { key: "S", keyCode: 83 },
      { key: "D", keyCode: 68 },
      { key: "F", keyCode: 70 },
      { key: "G", keyCode: 71 },
      { key: "H", keyCode: 72 },
      { key: "J", keyCode: 74 },
      { key: "K", keyCode: 75 },
      { key: "L", keyCode: 76 },
      { key: "Ñ", keyCode: 76 },
    ],
    [
      { key: "Enter", keyCode: 13 },
      { key: "Z", keyCode: 90 },
      { key: "X", keyCode: 88 },
      { key: "C", keyCode: 67 },
      { key: "V", keyCode: 86 },
      { key: "B", keyCode: 66 },
      { key: "N", keyCode: 78 },
      { key: "M", keyCode: 77 },
      { key: "Backspace", keyCode: 8 },
    ],
  ];

  const enterSvg = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="white"
      className="w-6 h-6"
    >
      <path d="M19,15L13,21L11.58,19.58L15.17,16H4V4H6V14H15.17L11.58,10.42L13,9L19,15Z" />
    </svg>
  );

  const backspaceSvg = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="white"
      className="w-6 h-6"
    >
      <path d="M19,15.59L17.59,17L14,13.41L10.41,17L9,15.59L12.59,12L9,8.41L10.41,7L14,10.59L17.59,7L19,8.41L15.41,12L19,15.59M22,3A2,2 0 0,1 24,5V19A2,2 0 0,1 22,21H7C6.31,21 5.77,20.64 5.41,20.11L0,12L5.41,3.88C5.77,3.35 6.31,3 7,3H22M22,5H7L2.28,12L7,19H22V5Z" />
    </svg>
  );

  return (
    <div>
      {rows.map((row, index) => (
        <div key={index} className="my-2">
          <div id={"row" + index} className="flex justify-center gap-2">
            <Grid
              rows={row}
              currentRow={index}
              word={word}
              key={"row" + index}
            />
          </div>
        </div>
      ))}
      <div className="flex-initial my-8">
        {keyboardRows.map((row, rowIndex) => (
          <div
            key={rowIndex}
            className="flex items-center justify-center gap-2 my-2"
          >
            {row.map((key) => (
              <button
                id={key.key}
                key={key.key}
                className={`${keys} flex border border-neutral-500 border-2 bg-neutral-500 flex items-center text-white font-semibold justify-center sm:w-14 sm:h-14 rounded-md`}
                onClick={() => handleKeyDown(key)}
              >
                {key.key == "Backspace"
                  ? backspaceSvg
                  : key.key == "Enter"
                  ? enterSvg
                  : key.key}
              </button>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}
