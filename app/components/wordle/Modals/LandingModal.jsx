import { useEffect } from "react";
import {
  modalCorrectCell,
  modalIncorrectCell,
  modalCloseEnoughCell,
} from "../../../wordle/wordle.module.css";

export default function LandingModal() {
  const cellStyle = `border border-neutral-500 border-2 flex items-center justify-center w-16 h-16 rounded-sm`;
  const demoWord1 = ["P", "A", "P", "A", "S"];
  const demoWord2 = ["J", "U", "E", "G", "O"];
  const demoWord3 = ["S", "O", "D", "A", "S"];

  function returnRows(word, typeOfHighLight, place) {
    return (
      <div className="flex justify-center gap-2 my-4">
        {word.map((letter, index) => {
          return (
            <div
              key={index}
              className={
                index === place ? `${typeOfHighLight} ${cellStyle}` : cellStyle
              }
            >
              <a className="uppercase font-bold text-xl text-white">{letter}</a>
            </div>
          );
        })}
      </div>
    );
  }

  function title3(text) {
    return (
      <div className="grid grid-cols-6">
        <ul className="col-start-2 col-span-4 font-bold text-md text-neutral-300 my-2 flex justify-center items-center">
        <li>{text}</li>
      </ul>
      </div>
      
    );
  }

  useEffect(() => {
    my_modal_1.showModal();
  });

  return (
    <dialog id="my_modal_1" className="modal">
      <form
        method="dialog"
        className="modal-box h-auto rounded-md bg-neutral-900 gap-4 "
      >
        <h1 className="flex justify-center font-bold text-3xl text-white my-4">Cómo jugar</h1>
        <h2 className="font-semibold text-xl text-neutral-200 my-3 flex justify-center">
          Adivina la palabra en 6 intentos
        </h2>
        {title3("•	Acierta y la casilla se pondrá en verde")}

        {returnRows(demoWord1, modalCorrectCell, 0)}

        {title3("•	Falla y se reflejará en la casilla")}

        {returnRows(demoWord2, modalIncorrectCell, 2)}

        {title3(
          "•	Tendrás una pista si aciertas en la pero no en la posición"
        )}
				 {/* <li className="font-bold text-md text-neutral-300 mb-5 flex justify-center mx-6"></li> */}
        {returnRows(demoWord3, modalCloseEnoughCell, 4)}
        <div className="font-bold text-md text-neutral-300 mb-5 flex justify-center mx-4">
          Todas las palabras se encuentran en español sin carácteres especiales
        </div>
        <div className="flex items-center justify-center">
          <div
            className="btn rounded-sm text-2xl border border-2 border-neutral-800 hover:bg-emerald-800 bg-emerald-600   text-neutral-200 hover:text-neutral-900 my-2 w-full h-2/5"
            onClick={() => my_modal_1.close()}
          >
            jugar!
          </div>
        </div>
      </form>

      <form method="dialog" className="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  );
}
