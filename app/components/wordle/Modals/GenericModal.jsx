import React, { useEffect } from "react";
import { winAnimation } from "../../../wordle/wordle.module.css";

function winMessage(tries) {
  if (tries + 1 === 1) return "Compraría la lotería si fuese tú";
  if (tries + 1 === 2) return "Increíble!";
  if (tries + 1 === 3 || tries + 1 === 4) return "Bien jugado";
  if (tries + 1 === 5) return "Estuvo cerca!";
  if (tries + 1 === 6) return "Ni siquiera me preocupé";
}

const GenericModal = ({ tries, gamestatus, word }) => {
  useEffect(() => {
    myModal.showModal();
  });

  return (
    <dialog id="myModal" className="modal">
      <form method="dialog" className="modal-box h-auto rounded-md bg-neutral-900 gap-4">
        <h1 className={`font-bold text-4xl text-white my-4 flex justify-center text-yellow-500 ${gamestatus ? winAnimation : ""}`}>
          {gamestatus ? "GANASTE!" : "Buen intento!"}
        </h1>
        <div className="font-bold text-md text-neutral-400 my-2 mx-6 flex justify-center">
          {gamestatus ? winMessage(tries) : `La palabra es`}
        </div>
        {
        gamestatus?  (
          <React.Fragment>
            <h1 className="flex justify-center font-semibold text-lg text-neutral-200 my-3">
              Lo has conseguido en
            </h1>
            <h1 className="flex justify-center font-bold text-3xl text-white my-4">
              {tries + 1}
            </h1>
            <h1 className="flex justify-center font-semibold text-lg text-neutral-200 my-3">
              {tries + 1 !== 1 ? "intentos" : "intento"}
            </h1>
          </React.Fragment>
        ): 
        <h1 className="flex justify-center font-bold text-3xl text-white my-4">
        {word}
        </h1>
      }
        <div className="flex justify-center items-center">
          <div
            className="btn rounded-sm text-2xl border border-2 border-neutral-800 hover:bg-emerald-800 bg-emerald-500 text-neutral-200
            hover:text-neutral-900 my-2 w-full h-2/5"
            onClick={() => window.location.reload()}
          >
            Volver a Jugar
          </div>
        </div>
      </form>
      <form method="dialog" className="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  );
};

export default GenericModal;
