"use client";
import { useEffect } from "react";
import Carousel from "../Carousel/Carousel";

export default function LandingModal() {
  function title3(text) {
    return (
      <div className="grid grid-cols-6">
        <ul className="col-start-2 col-span-4 font-bold text-md text-neutral-300 my-2 flex justify-center items-center">
          <li>{text}</li>
        </ul>
      </div>
    );
  }

  useEffect(() => {
    my_modal_1.showModal();
  });

  return (
    <dialog id="my_modal_1" className="modal">
      <div className="modal-box h-auto rounded-md bg-neutral-900 w-11/12 overflow-x-hidden">
        <h1 className="flex justify-center font-bold text-3xl text-white my-4">
          Controles
        </h1>
        <Carousel />

        <div className="flex items-center justify-center">
          <div
            className="btn rounded-sm text-2xl border border-2 border-neutral-800 hover:bg-emerald-800 bg-emerald-600   text-neutral-200 hover:text-neutral-900 my-2 w-full h-2/5"
            onClick={() => my_modal_1.close()}
          >
            jugar!
          </div>
        </div>
      </div>

      <form method="dialog" className="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  );
}
