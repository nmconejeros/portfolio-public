import style from "./game.module.css";

const previewGridSize = 4;

let preview = new Array(previewGridSize);

for (let rowIndex = 0; rowIndex < previewGridSize; rowIndex++) {
  preview[rowIndex] = new Array(previewGridSize);
  for (let colIndex = 0; colIndex < previewGridSize; colIndex++) {
    preview[rowIndex][colIndex] = 0;
  }
}
export default function PreviewGrid() {
  return (
    <div className="min-[992px]:w-full max-[991px]:w-1/3  flex justify-center">
      <div id="preview">
        {preview.map((row, rowIndex) => {
          return (
            <div
              key={"previewRow" + rowIndex}
              className="previewRows flex items-center justify-center w-fit "
            >
              {row.map((cell, cellIndex) => {
                return (
                  <div
                    key={"previewCell" + rowIndex + "," + cellIndex}
                    className={`${style.cell} min-[992px]:w-8 min-[992px]:h-8 max-[991px]:w-5 max-[991px]:h-5 border border-neutral-600 border-1`}
                  ></div>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
}
