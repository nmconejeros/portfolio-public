import style from "./game.module.css";

export default function GameGrid() {
  const cols = 10;
  const rows = 15;
  let boardItems = new Array(rows);

  for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
    boardItems[rowIndex] = new Array(cols);
    for (let colIndex = 0; colIndex < cols; colIndex++) {
      boardItems[rowIndex][colIndex] = 0;
    }
  }
  return (
    <div id="grid">
      {boardItems.map((row, rowIndex) => {
        return (
          <div
            id={"row" + rowIndex}
            key={"row" + rowIndex}
            className="row flex items-center justify-center w-fit "
          >
            {row.map((cell, cellIndex) => {
              return (
                <div
                  id={"cell" + rowIndex + "," + cellIndex}
                  key={"cell" + rowIndex + "," + cellIndex}
                  className={`${style.cell} cell min-[992px]:w-10 min-[992px]:h-10 max-[991px]:w-9 max-[991px]:h-9 border border-neutral-600 border-1`}
                ></div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
}
