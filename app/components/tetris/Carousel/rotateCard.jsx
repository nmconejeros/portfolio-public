"use client";
import { keyPress, rotate } from "../Modals/modal.module.css";
import { keyUpSVG, touchSVG } from "../Modals/SVGs";
import { useEffect } from "react";

export default function RotateCard({ dotFn }) {
  useEffect(() => {
    const cardsRef = document.querySelectorAll(".carousel-slide");

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            dotFn(entry.target.id.match(/\d/)[0]);
          }
        });
      },
      {
        threshold: 1,
      }
    );

    cardsRef.forEach((element) => {
      observer.observe(element);
    });
  });

  return (
    <div className="w-full h-full flex-none">
      <div
        id="card1"
        className="carousel-slide flex justify-center items-center h-72"
      >
        {/* KeyUP SVG*/}
        <div
          className={`${keyPress} w-1/3 h-10 flex justify-center items-center`}
        >
          <div
            id="keyUpSVG"
            className="w-10 h-10 flex justify-center items-center "
          >
            {keyUpSVG}
          </div>
        </div>

        {/* Image */}
        <div className="w-1/3 h-12 flex justify-center items-center select-none ">
          <img className={`h-12  ${rotate}`} src="/Images/demopiece.png" />
        </div>

        {/* Touch SVG  */}
        <div className="w-1/3 h-10 flex justify-center items-center ">
          <div
            id="touchSVG"
            className={`${keyPress} w-8 h-8 flex justify-center items-center `}
          >
            {touchSVG}
          </div>
        </div>
      </div>

      <div className="font-bold text-md text-neutral-300 flex h-8 -mt-8 justify-center items-center">
        • Girar pieza
      </div>
    </div>
  );
}
