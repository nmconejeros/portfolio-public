"use client";
import { keyPress, touchSlideY, slideY } from "../Modals/modal.module.css";
import { keyDownSVG, touchSVG } from "../Modals/SVGs";
import { useEffect } from "react";

export default function MoveDownCard({ dotFn }) {
  useEffect(() => {
    const cardsRef = document.querySelectorAll(".carousel-slide");

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            dotFn(entry.target.id.match(/\d/)[0]);
          }
        });
      },
      {
        threshold: 1,
      }
    );

    cardsRef.forEach((element) => {
      observer.observe(element);
    });
  });

  return (
    <div className="w-full h-full flex-none">
      <div
        id="card3"
        className="carousel-slide flex justify-center items-center h-72"
      >
        <div className="w-1/3 h-10 flex justify-center items-center ">
          {/* keyDown SVG*/}
          <div
            id="keyDownSVG"
            className={`${keyPress} w-10 h-10 flex justify-center items-center `}
          >
            {keyDownSVG}
          </div>
        </div>

        {/* Image */}
        <div className="w-1/3 h-12 flex justify-center items-center select-none ">
          <img className={`h-12  ${slideY}`} src="/Images/demopiece.png" />
        </div>

        {/* Touch SVG  */}
        <div className="w-1/3 h-10 flex justify-center items-center ">
          <div
            className={`${touchSlideY} w-8 h-8 flex justify-center items-center`}
          >
            {touchSVG}
          </div>
        </div>
      </div>
      <div className="font-bold text-md text-neutral-300 h-8 -mt-8 flex justify-center items-center">
        • Acelerar caída
      </div>
    </div>
  );
}
