"use client";
import { keyPress, slideX, touchSlideX } from "../Modals/modal.module.css";
import { keyRightSVG, keyLeftSVG, touchSVG } from "../Modals/SVGs";
import { useEffect } from "react";

export default function MoveCard({ dotFn }) {
  useEffect(() => {
    const cardsRef = document.querySelectorAll(".carousel-slide");
    const key = document.getElementById("keyLeftSVG");
    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            dotFn(entry.target.id.match(/\d/)[0]);
          }
        });
      },
      {
        threshold: 1,
      }
    );

    cardsRef.forEach((element) => {
      observer.observe(element);
    });

    key.style.setProperty("--delay", `${2}s`);
  });

  return (
    <div className="w-full h-full flex-none">
      <div
        id="card2"
        className="carousel-slide flex justify-center items-center h-72"
      >
        <div className="h-10 w-1/3  ml-4 flex justify-center items-center">
          {/* KeyLeft SVG*/}
          <div
            id="keyLeftSVG"
            className={`${keyPress} w-10 h-10 flex justify-center items-center `}
          >
            {keyLeftSVG}
          </div>
          {/* KeyRight SVG */}
          <div
            id="keyRightSVG"
            className={`${keyPress} w-9 h-9 flex justify-center items-center `}
          >
            {keyRightSVG}
          </div>
        </div>

        {/* Image */}
        <div className="h-12 w-1/3 flex justify-center items-center select-none ">
          <img className={`h-12  ${slideX}`} src="/Images/demopiece.png" />
        </div>

        {/* Touch SVG   */}
        <div className=" h-10 w-1/3 flex justify-center items-center ">
          <div
            className={`${touchSlideX} w-8 h-8 flex justify-center items-center`}
          >
            {touchSVG}
          </div>
        </div>
      </div>
      <div className="font-bold text-md text-neutral-300 h-8 -mt-8 flex justify-center items-center">
        • Mover pieza
      </div>
    </div>
  );
}
