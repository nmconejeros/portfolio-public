"use client";
import RotateCard from "./rotateCard";
import Script from "next/script";
import { carousel, svg } from "./carousel.module.css";
import { useState } from "react";
import MoveCard from "./moveCard";
import MoveDownCard from "./moveDownCard";
import { arrowLeftSVG, arrowRightSVG } from "../Modals/SVGs";

export default function Carousel() {
  const [currentDot, setCurentDot] = useState("");

  /* Gets the curret slide on the viewport from the CarouselCard component*/
  const dotFn = (dot) => {
    setCurentDot(dot);
  };

  /*Applies styling to the dot */
  const dotSVG = (color = "#000000") => (
    <svg
      fill={color}
      width="64px"
      height="64px"
      viewBox="0 0 24.00 24.00"
      xmlns="http://www.w3.org/2000/svg"
      stroke="#000000"
      strokeWidth="1"
    >
      <path d="M12 18a6 6 0 100-12 6 6 0 000 12z"></path>
    </svg>
  );

  /* create an array of dots and 
    Gives styling depending on slide in the viewport
  */
  const dots = (cardsTotal) => {
    let dotArr = [];
    for (let i = 1; i <= cardsTotal; i++) {
      dotArr.push(
        <div
          id={`dot_${i}`}
          key={`dot_${i}`}
          className={`${svg} w-10 h-10 mb-1 `}
        >
          {currentDot == i ? dotSVG("#f5f5f5") : dotSVG()}
        </div>
      );
    }
    return dotArr;
  };

  return (
    <div
      id="carousel"
      className="min-[992px]:h-96 max-[991px]:h-96 relative flex justify-center items-center"
    >
      <div className={`${carousel} carousel-wrapper flex h-fit w-fit`}>
        <RotateCard dotFn={dotFn} />
        <MoveCard dotFn={dotFn} />
        <MoveDownCard dotFn={dotFn} />
        <Script src="/Scripts/Carousel/tetrisControls.js" />
      </div>

      <div
        className={`${
          currentDot == 3 ? "hidden" : ""
        } absolute right-0 flex justify-center z-40`}
      >
        <div id="moveForward" className="w-5 h-5 -mr-2">
          {arrowRightSVG}
        </div>
      </div>
      <div
        className={`${
          currentDot == 1 ? "hidden" : ""
        } absolute left-0 flex justify-center z-40`}
      >
        <div id="moveBackward" className="w-5 h-5 -ml-2">
          {arrowLeftSVG}
        </div>
      </div>

      <div className="absolute bottom-0 flex justify-center z-40">
        {dots(3)}
      </div>
    </div>
  );
}
