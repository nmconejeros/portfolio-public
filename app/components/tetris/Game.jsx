"use client";
import { game } from "../../tetris/tetris.module.css";
import LandingModal from "./Modals/LandingModal";
import GameGrid from "./GameGrid";

import PreviewGrid from "./PreviewGrid";

export default function Game() {
  return (
    <div className="min-[992px]:flex max-[991px]:flex-cols mt-12 justify-center items-center h-fit">
      <div className="min-[992px]:flex-cols max-[991px]:flex m-4 justify-center items-center">
        <button
          id="stopButton"
          className="min-[992px]:w-full w-12 h-12 my-4 font-semibold hidden flex justify-center items-center border border-emerald-500 rounded-lg text-emerald-500 hover:bg-emerald-500 hover:text-white"
        >
          Pause
        </button>

        <PreviewGrid />

        <div className="min-[992px]:w-full max-[991px]:w-1/3 my-4 flex justify-center">
          <button
            id="startButton"
            className="font-semibold p-2  border border-emerald-500 rounded-lg text-emerald-500 hover:bg-emerald-500 hover:text-white"
          >
            Start
          </button>
          <div
            id="pointsSection"
            className="p-2 hidden flex font-semibold gap-2"
          >
            <div>Score:</div>
            <div id="score" className="text-emerald-500"></div>
          </div>
        </div>
      </div>
      <div
        id="board"
        className={`${game} min-[992px]:my-12  min-[992px]:mx-12 max-[991px]:mt-2  max-[991px]:mb-2 h-fit w-fit`}
      >
        <LandingModal />
        <GameGrid />
      </div>
      <script
        type="module"
        src="/Scripts/Tetris/tetris.js"
        crossOrigin="anonymous"
        async
      />
    </div>
  );
}
