import { page, section, first_section } from "../../home.module.css";
import { fadeT, fadeB, fadeL } from "./about.module.css";
import CodeText from "./CodeText";

export default function AboutMe() {
  const linkedInSVG = (
    <svg
      className="w-8 h-8"
      viewBox="0 0 24.00 24.00"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      stroke="#10b981"
      strokeWidth="0.00024000000000000003"
    >
      <path
        d="M6.5 8C7.32843 8 8 7.32843 8 6.5C8 5.67157 7.32843 5 6.5 5C5.67157 5 5 5.67157 5 6.5C5 7.32843 5.67157 8 6.5 8Z"
        fill="#10b981"
      ></path>
      <path
        d="M5 10C5 9.44772 5.44772 9 6 9H7C7.55228 9 8 9.44771 8 10V18C8 18.5523 7.55228 19 7 19H6C5.44772 19 5 18.5523 5 18V10Z"
        fill="#10b981"
      ></path>
      <path
        d="M11 19H12C12.5523 19 13 18.5523 13 18V13.5C13 12 16 11 16 13V18.0004C16 18.5527 16.4477 19 17 19H18C18.5523 19 19 18.5523 19 18V12C19 10 17.5 9 15.5 9C13.5 9 13 10.5 13 10.5V10C13 9.44771 12.5523 9 12 9H11C10.4477 9 10 9.44772 10 10V18C10 18.5523 10.4477 19 11 19Z"
        fill="#10b981"
      ></path>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M20 1C21.6569 1 23 2.34315 23 4V20C23 21.6569 21.6569 23 20 23H4C2.34315 23 1 21.6569 1 20V4C1 2.34315 2.34315 1 4 1H20ZM20 3C20.5523 3 21 3.44772 21 4V20C21 20.5523 20.5523 21 20 21H4C3.44772 21 3 20.5523 3 20V4C3 3.44772 3.44772 3 4 3H20Z"
        fill="#10b981"
      ></path>
    </svg>
  );

  const gitLabSVG = (
    <svg
      className="w-8 h-8"
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0.5 8.5L7.5 14.5L14.5 8.5L12.5 0.5L10.5 6.5H4.5L2.5 0.5L0.5 8.5Z"
        stroke="#10b981"
        strokeLinejoin="round"
      ></path>
    </svg>
  );

  return (
    <div
      id="about"
      className={`${page} flex justify-center w-full max-[991px]:h-auto min-[992px]:h-screen ${first_section} `}
    >
      <div
        className={`${section} min-[992px]:flex min-[992px]:flex-col min-[992px]:justify-center min-[992px]:items-center  min-[992px]:w-full max-[991px]:drop-shadow-2xl bg-neutral-900 max-[991px]:auto w-11/12`}
      >
        <div className="ml-8 my-20">
          <h1
            className={`${fadeT} text-4xl text-white font-bold flex justify-left `}
          >
            Hola!
          </h1>

          <h2
            className={`${fadeL} text-2xl text-emerald-500 font-bold flex justify-left ml-2 `}
          >
            Soy
          </h2>

          <h2
            className={`${fadeB} text-2xl text-white font-bold flex justify-left ml-4 `}
          >
            Nicolás Conejeros
          </h2>
        </div>

        <CodeText />

        <div className=" mt-12 mb-8 flex justify-center">
          <a
            className="btn rounded-sm bg-emerald-500 text-neutral-900  hover:active:bg-emerald-800 hover:active:text-neutral-200"
            href={"/CV/CV.pdf"}
            download="CV"
            target="_blank"
            rel="noreferrer"
          >
            Currículum
          </a>
        </div>
        <div className=" mb-12 flex justify-center gap-2">
          <a
            href="https://www.linkedin.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            {linkedInSVG}
          </a>

          <a
            href="https://gitlab.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            {gitLabSVG}
          </a>
        </div>
      </div>
    </div>
  );
}
