"use client";
import { codeDiv, codeH } from "./about.module.css";
import { useState, useEffect } from "react";

export default function CodeText() {
  const targetString = "Ingeniero en Computación e Informática";
  const [outputString, setOutputString] = useState("");
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (currentIndex < targetString.length) {
        const charToAdd = targetString[currentIndex];
        setOutputString((prevString) => prevString + charToAdd);
        setCurrentIndex((prevIndex) => prevIndex + 1);
      } else {
        clearInterval(interval); // Stops the interval when the string is complete
      }
    }, 50); // Add a character every 50 ms

    return () => {
      clearInterval(interval); // Cleanup when the component unmounts
    };
  }, [currentIndex, targetString]);

  return (
    <div className="flex justify-center my-8 min-[992px]:h-96 h-56 min-[992px]:w-96 w-full">
      <div className="font-mono text-neutral-300 w-10/12 ">
        <div className={`${codeDiv} text-sm lg:text-lg my-2 `}>
          {"<"}
          <span className="text-sky-500 mr-1">div</span>
          <span className="text-sky-300 ">id</span>
          {"="}
          <span className="text-orange-500 mr-1">{"'message'"}</span>
          <span className="text-sky-300 ">className</span>
          {"="}
          <span className="text-orange-500 mr-1">{"'flex'"}</span>
          {">"}
        </div>

        <div className={`${codeH} text-sm lg:text-lg ml-4  my-2`}>
          {"<"}
          <span className="text-sky-500 mr-1">h1</span>
          <span className="text-sky-300 ">className</span>
          {"="}
          <span className="text-orange-500 mr-1">{"'text-2xl'"}</span>
          {">"}
        </div>

        <div
          className={` text-white text-lg lg:text-xl ml-8 flex-none flex-col`}
        >
          {outputString}
        </div>

        <div className={`${codeH} text-sm lg:text-lg ml-4  `}>
          {"</"}
          <span className="text-sky-500 mr-1">h1</span>
          {">"}
        </div>

        <div className={`${codeDiv} text-sm lg:text-lg my-2`}>
          {"</"}
          <span className="text-sky-500 mr-1">div</span>
          {">"}
        </div>
      </div>
    </div>
  );
}
