"use client";
import CarouselCard from "./CarouselCard";
import Script from "next/script";
import { carousel, svg } from "./carousel.module.css";
import { useState } from "react";

export default function Carousel() {
  const [currentDot, setCurentDot] = useState("");

  /* Gets the curret slide on the viewport from the CarouselCard component*/
  const dotFn = (dot) => {
    setCurentDot(dot);
  };

  /*Applies styling to the dot */
  const dotSVG = (color = "#000000") => (
    <svg
      fill={color}
      width="64px"
      height="64px"
      viewBox="0 0 24.00 24.00"
      xmlns="http://www.w3.org/2000/svg"
      stroke="#000000"
      strokeWidth="1"
    >
      <path d="M12 18a6 6 0 100-12 6 6 0 000 12z"></path>
    </svg>
  );

  /* create an array of dots and 
    Gives styling depending on slide in the viewport
  */
  const dots = (cardsTotal) => {
    let dotArr = [];
    for (let i = 1; i <= cardsTotal; i++) {
      dotArr.push(
        <div
          id={`dot_${i}`}
          key={`dot_${i}`}
          className={`${svg} w-10 h-10 mb-1 `}
        >
          {currentDot == i ? dotSVG("#f5f5f5") : dotSVG()}
        </div>
      );
    }
    return dotArr;
  };

  return (
    <div id="carousel" className="min-[992px]:h-full max-[991px]:h-96">
      <div className={`${carousel} static carousel-wrapper flex h-full`}>
        {currentDot ? (
          <Script src="/Scripts/Carousel/carouselControls.js" />
        ) : (
          ""
        )}

        <CarouselCard
          cardId={1}
          titleLeft="Esp"
          titleRight="Eng"
          bottomText="Hablo inglés de manera fluida"
          bgColor="bg-emerald-500"
          dotFn={dotFn}
        />
        <CarouselCard
          cardId={2}
          titleMiddle="Sociable"
          bottomText="Porque las risas nunca faltan"
          bgColor="bg-yellow-500"
          dotFn={dotFn}
        />
        <CarouselCard
          cardId={3}
          titleMiddle="Comprometido"
          bottomText="Con mi equipo y las metas"
          bgColor="bg-red-500"
          dotFn={dotFn}
        />
      </div>

      <div className="absolute left-0 bottom-0 right-0 flex justify-center z-40">
        {dots(3)}
      </div>
    </div>
  );
}
