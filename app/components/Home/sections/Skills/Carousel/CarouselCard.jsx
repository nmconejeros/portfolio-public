"use client";
import style from "../skills.module.css";
import { useEffect, useState } from "react";
import { addAnimationCarousel } from "../Scripts/animationScript.js";

export default function CarouselCard({
  cardId = 1,
  titleLeft = "",
  titleRight = "",
  titleMiddle = "",
  bottomText = "",
  bgColor = "emerald-500",
  dotFn,
}) {
  const [hash, setHash] = useState("");

  useEffect(() => {
    const cardsRef = document.querySelectorAll(".carousel-slide");

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            dotFn(entry.target.id.match(/\d/)[0]);
            setHash(entry.target.id);
          }
        });
      },
      {
        threshold: 1,
      }
    );

    cardsRef.forEach((element) => {
      observer.observe(element);
    });
  });

  /*The slides have the current structure:
    if titleMiddle => titleL and titleR wont appear
    if titleL and titleR => titleMiddle == / 
  */
  const returnStyle = (typeOfAnimation) => {
    if ("card_" + cardId == hash) {
      if (titleMiddle == "") {
        addAnimationCarousel(cardId);
      }

      return typeOfAnimation;
    } else return "opacity-0";
  };

  return (
    // Card
    <div
      id={`card_${cardId}`}
      className={` ${bgColor} carousel-slide w-full h-full flex-none`}
    >
      {/* Card Content */}
      <div
        id={`card_content${cardId}`}
        className={`${returnStyle(
          style.shaking
        )} flex justify-center items-center w-full h-3/5`}
      >
        {/* Card left title */}
        <h1
          id={`leftText${cardId}`}
          className={`${returnStyle(
            style.fadeL
          )}  text-5xl font-bold font-mono text-white mx-1`}
        >
          {titleLeft}
        </h1>

        {/* Card Middle title */}
        <h1
          id={`middleText${cardId}`}
          className={`${returnStyle(style.fadeT)} ${
            titleMiddle ? "flex justify-center mx-4" : "mx-1"
          } max-[399px]:text-4xl min-[400px]:text-5xl font-bold font-mono text-white `}
        >
          {titleMiddle ? titleMiddle : "/"}
        </h1>

        {/* Card Right title */}
        <h1
          id={`rightText${cardId}`}
          className={`${returnStyle(
            style.fadeR
          )} text-5xl font-bold font-mono text-white mx-1`}
        >
          {titleRight}
        </h1>
      </div>

      {/* Bottom text */}
      <h2
        id={`bottomText${cardId}`}
        className={`${returnStyle(
          style.fadeB
        )} text-xl font-bold text-neutral-900 flex justify-center items-center mx-4`}
      >
        {bottomText}
      </h2>
    </div>
  );
}
