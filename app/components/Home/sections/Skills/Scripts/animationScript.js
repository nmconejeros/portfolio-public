function addAnimationCarousel(cardId) {
  let right = document.getElementById(`rightText${cardId}`);
  let left = document.getElementById(`leftText${cardId}`);
  let middle = document.getElementById(`middleText${cardId}`);
  let card_content = document.getElementById(`card_content${cardId}`);
  let bottom_text = document.getElementById(`bottomText${cardId}`);

  right.style.setProperty("--delay", `${0.3}s`);
  left.style.setProperty("--delay", `${0}s`);
  middle.style.setProperty("--delay", `${0.7}s`);
  card_content.style.setProperty("--delay", `${0.85}s`);
  bottom_text.style.setProperty("--delay", `${1}s`);
}

export { addAnimationCarousel };
