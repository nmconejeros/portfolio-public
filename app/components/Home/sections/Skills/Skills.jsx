"use client";
import { second_section, page, section } from "../../home.module.css";
import { frontImg, fadeT } from "./skills.module.css";
import Carousel from "./Carousel/Carousel";
import { useEffect } from "react";

export default function Skills() {
  useEffect(() => {
    const stops = document.querySelectorAll(".stop");

    const stopObserver = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            entry.target.classList.add(fadeT);
          } else entry.target.classList.remove(fadeT);
        });
      },
      {
        threshold: 0.5,
      }
    );

    stops.forEach((element) => {
      stopObserver.observe(element);
    });
  });

  function uniName() {
    return (
      <span>
        <span className="text-orange-500">U</span>
        <span className="text-sky-600">niversidad de </span>
        <span className="text-orange-500">S</span>
        <span className="text-sky-600">antiago de </span>
        <span className="text-orange-500">C</span>
        <span className="text-sky-600">hile</span>
      </span>
    );
  }

  function knowledgeArea1() {
    return <span className="text-emerald-500">FullStack</span>;
  }
  function knowledgeArea2() {
    return <span className="text-red-500 ml-1">QA</span>;
  }

  const firstParagraph = () => {
    return (
      <span>
        Egresado de la {uniName()}, siempre he sido interesado por cosas
        relacionadas a diversas áreas de la computación, mis conocimientos
        comprenden áreas de desarrollador {knowledgeArea1()} y de
        {knowledgeArea2()} Automation.
      </span>
    );
  };

  const secondParagraph =
    "Me encanta trabajar en equipo y cumplir las metas. Mis Hobbies incluyen realizar calistenia, origami y dibujar.";

  return (
    <>
      {/* Section with the carousel */}
      <div
        id="skills"
        className={`${page} ${second_section} flex  justify-center  items-center`}
      >
        <div className="flex items-center max-[991px]:flex-col w-full">
          <div
            className={`min-[992px]:bg-neutral-800 min-[992px]:w-6/12 min-[992px]:h-96 max-[991px]:h-32 max-[991px]:w-11/12 drop-shadow-2xl flex items-center justify-center`}
          >
            {/* First stop */}
            <h1
              id="firstStop"
              className={`opacity-0 stop text-2xl text-white font-bold`}
            >
              Soft Skills
            </h1>
          </div>
          <div
            className={`${section} bg-emerald-500 drop-shadow-2xl max-[991px]:w-11/12 min-[992px]:w-6/12 min-[992px]:h-96  `}
          >
            <Carousel />
          </div>
        </div>
      </div>

      {/* Second stop and title of the next section */}
      <div id="about_me">
        <div className="min-[992px]:mx-32 min-[992px]:my-32 max-[991px]:mx-8 max-[991px]:my-12  max-[991px]:justify-center flex  ">
          <h1 className={`opacity-0 stop text-4xl text-emerald-500 font-bold `}>
            .
          </h1>
          <h1
            id="secondStop"
            className={`opacity-0 stop text-4xl text-neutral-200 font-bold `}
          >
            Acerca de mí
          </h1>
        </div>

        {/* Last section */}
        <div className={`${page} flex justify-center items-center`}>
          <div
            className={`${section} min-[992px]:h-auto min-[992px]:px-32 max-[991px]:w-11/12 max-[991px]:border max-[991px]:border-emerald-500`}
          >
            {/* Text */}
            <div className="min-[992px]:flex">
              <div className="min-[992px]:mx-32 min-[992px]:my-28 min-[992px]:w-6/12 max-[991px]:mt-12 max-[991px]:mx-8">
                <div className="min-[992px]:text-2xl max-[991px]:text-lg mb-2 text-neutral-300 font-medium">
                  {firstParagraph()}
                </div>
                <div className="min-[992px]:text-2xl max-[991px]:text-lg mb-24 text-neutral-300 font-medium">
                  {secondParagraph}
                </div>
              </div>

              {/* Image */}
              <div className="flex justify-center items-center">
                <div
                  className={`${frontImg} bg-contain bg-center z-50 mb-12 min-[992px]:w-72 min-[992px]:h-72 max-[991px]:w-56 max-[991px]:h-56 rounded-lg`}
                  style={{ backgroundImage: `url(/Images/selfie.jpeg)` }}
                ></div>
                <div className="z-0 absolute min-[992px]:w-72 min-[992px]:h-72 max-[991px]:w-56 max-[991px]:h-56 ml-10  border border-emerald-500 rounded-lg"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
