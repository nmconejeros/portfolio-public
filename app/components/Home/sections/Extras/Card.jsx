"use client";
import { section } from "../../home.module.css";

export default function Card({
  Title = "Worde",
  Desc = "asdfa",
  Url = "/wordle",
}) {
  function addDataToGamePreview() {
    const cardTitle = document.getElementById("previewTitle");
    const cardDesc = document.getElementById("preivewDesc");
    cardTitle.textContent = Title;
    cardDesc.textContent = Desc;
  }
  return (
    <div
      className={`${section} min-[992px]:w-72 min-[992px]:h-72 max-[991px]:w-full max-[991px]:h-56 bg-neutral-900 hover:bg-neutral-500 rounded-lg z-50 my-12`}
      onMouseEnter={addDataToGamePreview}
    >
      <a
        className="text-4xl font-semibold flex justify-center items-center h-full text-white"
        href={Url}
      >
        {Title}
      </a>
    </div>
  );
}
