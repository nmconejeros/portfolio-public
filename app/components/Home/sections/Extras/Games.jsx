import { page } from "../../home.module.css";
import Card from "./Card";

export default function Games() {
  let wordleTitle = "Wordle";
  let wordleDesc = "Puedes descubrir la palabra en menos de 6 intentos?";
  let tetrisTitle = "Tetris";
  let tetrisDesc = "Prueba tú habilidad en un clásico de clásicos";

  return (
    <div
      className={`${page} min-[992px]:px-32 max-[991px]:flex max-[991px]:items-center max-[991px]:justify-center flex`}
    >
      <div
        className={`min-[992px]:mx-32 min-[992px]:h-auto min-[992px]:w-fit max-[991px]:w-11/12`}
      >
        <div className="flex items-center justify-center">
          <div className="text-2xl font-semibold text-white">Juegos</div>
        </div>
        <div className="min-[992px]:flex w-full gap-8">
          <Card Title={wordleTitle} Desc={wordleDesc} Url="/wordle" />
          <Card Title={tetrisTitle} Desc={tetrisDesc} Url="/tetris" />
        </div>
      </div>
      <div className="max-[991px]:hidden flex justify-center items-center">
        <div className={`border border-white w-80 h-72 rounded-lg`}>
          <div className="card-content">
            <div
              id="previewTitle"
              className="text-4xl font-semibold flex justify-center my-16 text-emerald-500"
            >
              {wordleTitle}
            </div>
            <div
              id="preivewDesc"
              className="text-md font-semibold flex justify-center mx-8 text-white"
            >
              {wordleDesc}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
