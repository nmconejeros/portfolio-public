import { third_section } from "../../home.module.css";
import Games from "./Games";

export default function Skills() {
  return (
    <div
      id="extras"
      className={` min-[992px]:p-3 min-[992px]:justify-center  max-[991px]:h-auto  ${third_section}`}
    >
      {/* Second stop and title of the next section */}
      <div className="min-[992px]:ml-32 min-[992px]:my-32 max-[991px]:mx-8 max-[991px]:my-12  max-[991px]:justify-center flex">
        <h1 className={`opacity-0 stop text-4xl text-emerald-500 font-bold `}>
          .
        </h1>
        <h1
          id="secondStop"
          className={`opacity-0 stop text-4xl text-neutral-200 font-bold `}
        >
          Extras
        </h1>
      </div>

      {/* Last section */}
      <Games />
    </div>
  );
}
