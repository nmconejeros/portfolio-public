"use client";
import { useEffect } from "react";
import About from "./sections/aboutMe/About";
import Skills from "./sections/Skills/Skills";
import Extras from "./sections/Extras/Extras";

export default function Home() {
  useEffect(() => {
    //gets all the sections by their id
    const sections = document.querySelectorAll(
      "#about, #skills, #about_me, #extras"
    );

    const sectionObserver = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            entry.target.scrollIntoView({
              behavior: "smooth",
              block: "nearest",
            });
          }
        });
      },
      {
        threshold: 0.5,
      }
    );

    sections.forEach((section) => {
      sectionObserver.observe(section);
    });
  });

  return (
    <div className={`items-center justify-center`}>
      <About />
      <Skills />
      <Extras />
    </div>
  );
}
