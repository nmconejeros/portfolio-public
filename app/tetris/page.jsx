import Game from "../components/tetris/Game";
import {page} from "./tetris.module.css"

export default function Tetris() {
  return (
    <div className={`${page}  bg-neutral-900 flex justify-center `}>
      <Game  />
    </div>
  );
}
