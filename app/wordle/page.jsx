"use client";

// Import necessary modules and components
import React, { useState } from "react";
import Game from "../components/wordle/Game";
import LandingModal from "../components/wordle/Modals/LandingModal";
import GenericModal from "../components/wordle/Modals/GenericModal";
import { game } from "./wordle.module.css";

export default function Wordle() {
  // Define state variables
  const [word, setWord] = useState("");
  const [tries, setTries] = useState(0);
  const [gameStatus, setGameStatus] = useState(false);
  const [isGameDone, setIsGameDone] = useState(false);

  // Function to update game state
  const updateGameState = (
    triesFromChild,
    gameStatusFromChild,
    wordFromChild,
    isGameDone
  ) => {
    setTries(triesFromChild);
    setGameStatus(gameStatusFromChild);
    setWord(wordFromChild);
    setIsGameDone(isGameDone);
  };

  return (
    <div className={`${game} mt-16 flex items-center justify-center bg-neutral-900`}>
      {/* Render LandingModal if the game is not done */}
      {!isGameDone && <LandingModal />}

      {/* Render Game component */}
      <Game updateGameState={updateGameState} />

      {/* Render GenericModal if the game is done */}
      {isGameDone && (
        <GenericModal tries={tries} gamestatus={gameStatus} word={word} />
      )}
    </div>
  );
}
