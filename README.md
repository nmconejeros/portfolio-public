# portfolio-react

Hello!
I'm Nicolás and this is my portfolio, i will be maintaining and updating this project in the future.
As for now, this is my personal project and its use its restricted to myself.

## Requirements

Must have installed [NodeJs](https://nodejs.org/es/download)/.

* NodeJS version v16.20.1 LTSC was used to make this project.
* Currently tested on Node versions 14-16.

## Installation

### Run

```bash
# Install dependencies
npm install

# Serve with hot reload at localhost:3000
npm run dev

# build for production and launch server
$ npm run build
$ npm run start
```
