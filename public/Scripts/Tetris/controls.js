//DOM variables
const board = document.querySelector("#board");
const swipeThreshold = document.querySelector(".cell").offsetWidth;

//Controls variables
let touchStartX = 0;
let touchEndX = 0;
let touchStartY = 0;
let touchEndY = 0;
let timer = null;
let movementX = 0;
let movementY = 0;
let howManyTimes = 0;
let keyDownListener;
let touchStartListener;
let touchMoveListener;
let touchEndListener;

function keyDownHandler(e, moveLeft, moveRight, moveDown, rotate) {
  // Assign functions to keyCodes
  if (e.keyCode === 37) {
    moveLeft();
  } else if (e.keyCode === 38) {
    rotate();
  } else if (e.keyCode === 39) {
    moveRight();
  } else if (e.keyCode === 40) {
    moveDown();
  }
}

// Initializes the variables on a touchscreen
function touchStartHandler(e) {
  touchStartX = e.touches[0].clientX; //position on the X axis of the touch
  touchEndX = touchStartX;
  touchStartY = e.touches[0].clientY;
  touchEndY = touchStartY;
}

// Updates the variables depending on the user's swipes
function touchMoveHandler(e, moveLeft, moveRight, moveDown) {
  touchEndX = e.touches[0].clientX; // Last position of the users current swipe
  touchEndY = e.touches[0].clientY;

  movementX = touchEndX - touchStartX; // How much the user has swiped along the x axis
  movementY = touchStartY - touchEndY; // How much the user has swiped along the y axis
  howManyTimes = Math.round(movementX / swipeThreshold); // How many squares the user has swiped through

  // If movement > cellWidth then move the current piece X cells on the swipes direction
  if (Math.abs(movementX) >= swipeThreshold) {
    // Resets the swipe, to prevent multiple inputs on a single swipe
    touchStartX = touchEndX;
    movementX = 0;

    let i = 0;
    do {
      if (howManyTimes < 0) moveLeft();
      else if (howManyTimes > 0) moveRight();
      i++;
    } while (i < Math.abs(howManyTimes));
  }

  // If swipe down -> pieces move down as long as the touch persists, with an 80ms delay
  if (movementY > 0 && Math.abs(movementY) >= swipeThreshold) {
    if (timer == null) {
      timer = setInterval(moveDown, 80);
    }
  }
}

// If the user just touched the screen -> rotate piece, clears current interval
function touchEndHandler(rotate) {
  if (touchStartX - touchEndX == 0) {
    rotate();
  }
  clearInterval(timer);
  timer = null;
}


// Controls init
export function controlsInit(moveLeft, moveRight, rotate, moveDown) {
  keyDownListener = (e) =>
    keyDownHandler(e, moveLeft, moveRight, moveDown, rotate);

  touchStartListener = (e) => touchStartHandler(e);

  touchMoveListener = (e) => touchMoveHandler(e, moveLeft, moveRight, moveDown);

  touchEndListener = () => touchEndHandler(rotate);

  document.addEventListener("keydown", keyDownListener);
  board.addEventListener("touchstart", touchStartListener, {
    passive: true,
  });

  board.addEventListener("touchmove", touchMoveListener, { passive: true });
  board.addEventListener("touchend", touchEndListener);
}


// Removes Event listeners
export function removeControls() {
  document.removeEventListener("keydown", keyDownListener);
  board.removeEventListener("touchstart", touchStartListener);
  board.removeEventListener("touchmove", touchMoveListener);
  board.removeEventListener("touchend", touchEndListener);
}
