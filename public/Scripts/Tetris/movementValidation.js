const BOARD_ROWS = 14;
const BOARD_COLUMNS = 9;

//Checks if a cell is taken
export function isCellTaken(rows, row, col) {
  return rows[row][col].classList.contains("taken");
}

// Check if the squares to the left of the colored cells are taken
export function checkLeft(
  currentShape,
  rotation,
  currentColumn,
  currentRow,
  rows
) {
  const tetrominoLength = currentShape.shape[0].length;

  for (let rowIndex = 0; rowIndex < tetrominoLength; rowIndex++) {
    for (let colIndex = 0; colIndex < tetrominoLength; colIndex++) {
      if (currentShape.shape[rotation][rowIndex][1 - colIndex] == 1) {
        const targetColumn = currentColumn - colIndex;
        const targetRow = currentRow + rowIndex;
        if (
          targetColumn < 0 ||
          targetRow > BOARD_ROWS ||
          isCellTaken(rows, targetRow, targetColumn)
        ) {
          return false;
        }
      }
    }
  }
  return true;
}

// Check if the squares to the right of the colored cells are taken
export function checkRight(
  currentShape,
  rotation,
  currentColumn,
  currentRow,
  rows
) {
  const tetrominoLength = currentShape.shape[0].length;

  for (let rowIndex = 0; rowIndex < tetrominoLength; rowIndex++) {
    for (let colIndex = 0; colIndex < tetrominoLength; colIndex++) {
      if (currentShape.shape[rotation][rowIndex][1 + colIndex] == 1) {
        const targetColumn = currentColumn + 2 + colIndex;
        const targetRow = currentRow + rowIndex;
        if (
          targetColumn > BOARD_COLUMNS ||
          targetRow > BOARD_ROWS ||
          isCellTaken(rows, targetRow, targetColumn)
        ) {
          return false;
        }
      }
    }
  }
  return true;
}

// Check if the cells below the colored cells are taken
export function checkDown(
  currentShape,
  rotation,
  currentColumn,
  currentRow,
  rows
) {
  const tetrominoLength = currentShape.shape[0].length;

  for (let rowIndex = 0; rowIndex < tetrominoLength; rowIndex++) {
    for (let colIndex = 0; colIndex < tetrominoLength; colIndex++) {
      if (currentShape.shape[rotation][colIndex][rowIndex] == 1) {
        const targetColumn = currentColumn + rowIndex;
        const targetRow = currentRow + 1 + colIndex;
        if (
          targetColumn > BOARD_COLUMNS ||
          targetRow > BOARD_ROWS ||
          isCellTaken(rows, targetRow, targetColumn)
        ) {
          return false;
        }
      }
    }
  }
  return true;
}

export function checkRotate(
  currentShape,
  nextRotation,
  currentColumn,
  currentRow,
  rows
) {
  const tetrominoLength = currentShape.shape[nextRotation].length;

  for (let rowIndex = 0; rowIndex < tetrominoLength; rowIndex++) {
    for (let colIndex = 0; colIndex < tetrominoLength; colIndex++) {
      if (currentShape.shape[nextRotation][rowIndex][colIndex] == 1) {
        const targetColumn = currentColumn + colIndex;
        const targetRow = currentRow + rowIndex;
        if (
          targetColumn > BOARD_COLUMNS ||
          targetColumn < 0 ||
          targetRow > BOARD_ROWS ||
          isCellTaken(rows, targetRow, targetColumn)
        ) {
          return false;
        }
      }
    }
  }
  return true;
}
