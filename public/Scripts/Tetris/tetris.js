import { Tetrominoes } from "./pieces.js";
import { points } from "./points.js";
import { controlsInit, removeControls } from "./controls.js";
import * as validate from "./movementValidation.js";
import * as render from "./rendering.js";

// DOM elements
let rows = Array.from(document.querySelectorAll(".row"));
let previewRows = Array.from(document.querySelectorAll(".previewRows"));
const startButton = document.querySelector("#startButton");
const stopButton = document.querySelector("#stopButton");
const pointsSection = document.querySelector("#pointsSection");
const AcumScore = document.querySelector("#score");

// Tetris game variables
let currentRow = 0;
let currentColumn = 4;
let random = 0;
let nextRandom = 0;
let currentShape;
let gameTick;
let rotation = 0;
let hasGameEnded = false;
let fallSpeed = 900;
points.pointsValue = 0;

// Convert rows to a 2D array
for (let i = 0; i < rows.length; i++) {
  rows[i] = Array.from(rows[i].childNodes);
}

// Convert previerRows to a 2D aray
for (let i = 0; i < previewRows.length; i++) {
  previewRows[i] = Array.from(previewRows[i].childNodes);
}

// Get a random Tetromino shape
function getPiece() {
  random = nextRandom;
  currentShape = Tetrominoes[random];
  nextRandom = Math.floor(Math.random() * Tetrominoes.length);
  render.clearGrid(previewRows);
  render.draw(previewRows, 0, 0, Tetrominoes[nextRandom], 0);
}

function adjustSpeed() {
  const level = Math.round(points.pointsValue / 30);

  // Default
  if (level <= 3 || level >= 24) {
    return;
  }

  // First level
  if (level > 3 && level < 8) {
    if (fallSpeed != 800) {
      clearInterval(gameTick);
      gameTick = null;
      fallSpeed = 800;
      gameTick = setInterval(moveDown, fallSpeed);
    }
    return;
  }
  // Second level
  else if (level >= 8 && level < 14) {
    if (fallSpeed != 600) {
      clearInterval(gameTick);
      gameTick = null;
      fallSpeed = 600;
      gameTick = setInterval(moveDown, fallSpeed);
    }

    return;
  }
  // Third level
  else if (level >= 14 && level < 19) {
    if (fallSpeed != 500) {
      clearInterval(gameTick);
      gameTick = null;
      fallSpeed = 400;
      gameTick = setInterval(moveDown, fallSpeed);
    }
    return;
  }
  // Fourth level
  else if (level >= 19 && level < 24) {
    if (fallSpeed != 100) {
      clearInterval(gameTick);
      gameTick = null;
      fallSpeed = 100;
      gameTick = setInterval(moveDown, fallSpeed);
    }
    return;
  }
}

// Checks if its possible to render a new piece on the starting position
function checkIfGameLost() {
  for (let colIndex = 4; colIndex < 8; colIndex++) {
    if (
      validate.isCellTaken(rows, 0, colIndex) &&
      validate.isCellTaken(rows, 1, colIndex)
    )
      gameEnd();
  }
}

/* Moves left.
    -Removes pieces from board
    -Checks if the move is possible
    -If the move is possible -> updates currentColumn and draws the tetrominoe
*/
function moveLeft() {
  render.remove(rows, currentColumn, currentRow, currentShape, rotation);
  if (
    validate.checkLeft(currentShape, rotation, currentColumn, currentRow, rows)
  ) {
    currentColumn--;
  }
  render.draw(rows, currentRow, currentColumn, currentShape, rotation);
}

/* Moves right.
    -Removes pieces from board
    -Checks if the move is possible
    -If the move is possible -> updates currentColumn and draws the tetrominoe
*/
function moveRight() {
  render.remove(rows, currentColumn, currentRow, currentShape, rotation);
  if (
    validate.checkRight(currentShape, rotation, currentColumn, currentRow, rows)
  ) {
    currentColumn++;
  }
  render.draw(rows, currentRow, currentColumn, currentShape, rotation);
}

/* Moves Down.
    -Removes pieces from board
    -Checks if the move is possible
    -If the move is possible -> updates currentRow and draws the tetrominoe
    -Else: 
        -Draws current tetrominoe
        -Checks if there are any rows filled from the currentPiece and clears them
        -Resets the values
        -If GameLost ==> gameEnd
        -Gets a new piece
    -If the game hasn´t ended: draws the new currentPiece
*/

function moveDown() {
  render.remove(rows, currentColumn, currentRow, currentShape, rotation);

  if (
    validate.checkDown(currentShape, rotation, currentColumn, currentRow, rows)
  ) {
    currentRow++;
  } else {
    render.draw(rows, currentRow, currentColumn, currentShape, rotation);

    let getpointsAndRows = render.cleanRow(
      rows,
      currentRow,
      points.pointsValue,
      score,
      currentShape
    );

    rows = getpointsAndRows[0];
    points.pointsValue = getpointsAndRows[1];
    currentColumn = 4;
    currentRow = 0;
    rotation = 0;

    checkIfGameLost();

    getPiece();
  }
  if (!hasGameEnded)
    render.draw(rows, currentRow, currentColumn, currentShape, rotation);
}

/* Rotates currentPiece.
    -Removes the tetrominoe from the board
    -Checks if the rotation is possible
    -If the rotation is possible
    -If current rotation == 3 goes back to the first rotation -> rotation = 0
*/
function rotate() {
  render.remove(rows, currentColumn, currentRow, currentShape, rotation);
  if (
    validate.checkRotate(
      currentShape,
      rotation == 3 ? 0 : rotation + 1,
      currentColumn,
      currentRow,
      rows
    )
  )
    rotation == 3 ? (rotation = 0) : rotation++;

  render.draw(rows, currentRow, currentColumn, currentShape, rotation);
}

function gameEnd() {
  // Stops the game from progressing
  clearInterval(gameTick);
  gameTick = null;
  hasGameEnded = true;

  // Resets the main variables
  currentShape = null;
  currentColumn = 4;
  currentRow = 0;
  rotation = 0;
  fallSpeed = 900;

  // Reset the grid
  render.clearGrid(rows);

  // Reset the buttons
  startButton.style.display = "";
  pointsSection.classList.add("hidden");
  stopButton.classList.add("hidden");

  removeControls();
}

// Removes current event listeners
function gameStop() {
  stopButton.addEventListener("click", () => {
    if (gameTick) {
      clearInterval(gameTick);
      gameTick = null;
      removeControls();
      stopButton.textContent = "Play";
    } else {
      stopButton.textContent = "Stop";
      gameTick = setInterval(moveDown, fallSpeed);
      controlsInit(moveLeft, moveRight, rotate, moveDown);
    }
  });
}

function gameStart() {
  startButton.addEventListener("click", () => {
    // Clears the grid
    render.clearGrid(rows);
    // Makes sure the game isn't running
    if (gameTick) {
      clearInterval(gameTick);
      gameTick = null;
    } else {
      //Starting values
      random = Math.floor(Math.random() * Tetrominoes.length);
      startButton.style.display = "none";
      pointsSection.classList.remove("hidden");
      stopButton.classList.remove("hidden");
      stopButton.textContent = "Stop";
      AcumScore.textContent = ` ${0}`;
      hasGameEnded = false;

      //Starting the game
      getPiece();
      render.draw(rows, currentRow, currentColumn, currentShape, rotation);
      gameTick = setInterval(moveDown, fallSpeed);
      controlsInit(moveLeft, moveRight, rotate, moveDown);
      // Custom event listener to adjust the game speed
      points.registerListener(function (val) {
        adjustSpeed();
      });
    }
  });
}

gameStart();
gameStop();
