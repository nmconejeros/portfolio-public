// Checks if the row passed is full
function isRowFull(row) {
  const isFull = (cell) => cell.classList.contains("taken");
  let ans = row.every(isFull);
  return ans;
}

//Adds or remove the styling from a cell
export function manageCellStyle(rowGrid, row, col, color, operation) {
  if (operation === "add") {
    rowGrid[row][col].style.backgroundColor = color;
    rowGrid[row][col].classList.add("taken");
  } else if (operation === "remove") {
    rowGrid[row][col].style.backgroundColor = "";
    rowGrid[row][col].classList.remove("taken");
  } else {
    console.log("operation on manageCellStyle not specified");
  }
}

export function clearGrid(grid) {
  for (let rowIndex = 0; rowIndex < grid.length; rowIndex++) {
    for (let colIndex = 0; colIndex < grid[0].length; colIndex++) {
      manageCellStyle(grid, rowIndex, colIndex, "", "remove");
    }
  }
}

// Draw the current Tetromino on the board
export function draw(rowGrid, startingRow, startingColumn, tetrominoe, spin) {
  const tetrominoLength = tetrominoe.shape[0].length;

  for (let rowIndex = 0; rowIndex < tetrominoLength; rowIndex++) {
    for (let colIndex = 0; colIndex < tetrominoLength; colIndex++) {
      if (tetrominoe.shape[spin][rowIndex][colIndex] == 1) {
        const targetColumn = startingColumn + colIndex;
        const targetRow = startingRow + rowIndex;
        manageCellStyle(
          rowGrid,
          targetRow,
          targetColumn,
          tetrominoe.color,
          "add"
        );
      }
    }
  }
}

// Remove the current Tetromino from the board
export function remove(rowGrid, currentColumn, currentRow, piece, rotation) {
  const tetrominoLength = piece.shape[0].length;

  for (let i = 0; i < tetrominoLength; i++) {
    for (let j = 0; j < tetrominoLength; j++) {
      if (piece.shape[rotation][i][j] == 1) {
        const targetColumn = currentColumn + j;
        const targetRow = currentRow + i;
        manageCellStyle(rowGrid, targetRow, targetColumn, "", "remove");
      }
    }
  }
}

// Cleans a row if all of its cells are taken
export function cleanRow(rows, currentRow, points, score, piece) {
  const pieceSize = piece.shape[0].length;
  for (let rowIndex = 0; rowIndex < pieceSize; rowIndex++) {
    const targetRow = currentRow + rowIndex;

    if (targetRow <= 14 && isRowFull(rows[targetRow])) {
      rows[targetRow].forEach((cell, cellIndex) => {
        manageCellStyle(rows, targetRow, cellIndex, "", "remove");
      });

      // Get the updated elements, could be done with an event listener
      let grid = document.querySelector("#grid");
      let nodeList = document.querySelectorAll(".row");

      // Update the grid
      let removedRow = nodeList[targetRow];
      let firstChild = grid.firstChild;

      // Remove the cleared Row and append it at the beginning
      grid.removeChild(removedRow);
      grid.insertBefore(removedRow, firstChild);

      // Update the current value of rows in the script
      let e = rows.splice(targetRow, 1);
      rows = [...e, ...rows];
      // Updating the score
      points += 30;
      score.textContent = ` ${points}`;
    }
  }
  return [rows, points];
}
