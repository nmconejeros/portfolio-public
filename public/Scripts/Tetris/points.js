// Custom event listener for points
const points = {
  aInternal: 10,
  aListener: function (val) {},
  set pointsValue(val) {
    this.aInternal = val;
    this.aListener(val);
  },
  get pointsValue() {
    return this.aInternal;
  },
  registerListener: function (listener) {
    this.aListener = listener;
  },
};

export {points};