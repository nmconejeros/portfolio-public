const wrapper = document.querySelector(".carousel-wrapper");
const slides = document.querySelectorAll(".carousel-slide");
let currentSlide = 0;
let touchStartX = 0;
let touchEndX = 0;
//Displays current slide
function showSlide(index) {
  const slideWidth = slides[0].offsetWidth; // Width of one slide
  const translateX = -index * slideWidth;

  wrapper.style.transform = `translateX(${translateX}px)`;
}

//Displays previous slide
function prevSlide() {
  currentSlide = Math.max(currentSlide - 1, 0);
  showSlide(currentSlide);
}

// Displays next slide
function nextSlide() {
  currentSlide = Math.min(currentSlide + 1, slides.length - 1);
  showSlide(currentSlide);
}

//Changes the slide every 3 seconds
let slideShow = setInterval(() => {
  if (currentSlide + 1 < slides.length) {
    nextSlide();
  } else {
    currentSlide = 0;
    touchStartX = 0;
    touchEndX = 0;
    showSlide(currentSlide);
  }
}, 3000);

// Mobile handler
wrapper.addEventListener(
  "touchstart",
  (e) => {
    touchStartX = e.touches[0].clientX; //position on the X axis of the touch
    clearInterval(slideShow); //Removes the slideshow feature
  },
  { passive: true }
);

// Mobile handler
wrapper.addEventListener(
  "touchmove",
  (e) => {
    touchEndX = e.touches[0].clientX; //How much the user has swiped along the x axis
  },
  { passive: true }
);

// Mobile handler
wrapper.addEventListener("touchend", () => {
  const slideWidth = slides[0].offsetWidth; // Width of one slide
  const swipeThreshold = slideWidth / 5; // Swipe threshold

  if (touchStartX - touchEndX > swipeThreshold) {
    nextSlide();
  } else if (touchEndX - touchStartX > swipeThreshold) {
    prevSlide();
  }
});

// Initial display of the first slide
showSlide(currentSlide);
